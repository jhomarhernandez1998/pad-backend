package com.padPeru.backend.padPeru.docs;

import java.util.ArrayList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
 
@Configuration
public class SwaggerConfig {
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(DEFAULT_API_INFO);
	}
	
	public static final Contact DEFAULT_CONTACT_INFO = new Contact("PAD Network", "http://localhost:8080", "sistemas@pad.com");
	
	public static final ApiInfo DEFAULT_API_INFO = new ApiInfo("PAD API Documentation", "PAD BackEnd", "1.0", "", DEFAULT_CONTACT_INFO, "", "", new ArrayList<>());
}
