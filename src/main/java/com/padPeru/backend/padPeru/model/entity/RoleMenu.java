package com.padPeru.backend.padPeru.model.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import com.padPeru.backend.padPeru.model.entity.key.RoleMenuKey;

@Entity
@Table(name = "roles_menus")
public class RoleMenu {
	@Id
	@EmbeddedId
	private RoleMenuKey id;
	
	@ManyToOne
	@MapsId("role_id")
	@JoinColumn(name = "role_id")
	private Role role;
	
	@ManyToOne
	@MapsId("menu_id")
	@JoinColumn(name = "menu_id")
	private Menu menu;
	
	@Column(columnDefinition = "boolean default false")
	private boolean disabled;

	public RoleMenu() {
		// super();
		// TODO Auto-generated constructor stub
	}

	public RoleMenu(RoleMenuKey id, Role role, Menu menu, boolean disabled) {
		super();
		this.id = id;
		this.role = role;
		this.menu = menu;
		this.disabled = disabled;
	}
	
	/**
	 * @return the id
	 */
	public RoleMenuKey getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(RoleMenuKey id) {
		this.id = id;
	}

	/**
	 * @return the role
	 */
	public Role getRole() {
		return role;
	}

	/**
	 * @param role the role to set
	 */
	public void setRole(Role role) {
		this.role = role;
	}

	/**
	 * @return the menu
	 */
	public Menu getMenu() {
		return menu;
	}

	/**
	 * @param menu the menu to set
	 */
	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	/**
	 * @return the disabled
	 */
	public boolean isDisabled() {
		return disabled;
	}

	/**
	 * @param disabled the disabled to set
	 */
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}
	
	@Override
	public String toString() {
		return "RoleMenu [id=" + id + ", role=" + role + ", menu=" + menu + ", disabled=" + disabled + "]";
	}
}
