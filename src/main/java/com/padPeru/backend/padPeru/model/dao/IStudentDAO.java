package com.padPeru.backend.padPeru.model.dao;

import org.springframework.data.repository.CrudRepository;

import com.padPeru.backend.padPeru.model.entity.Student;

public interface IStudentDAO extends CrudRepository<Student, Long>{

}
