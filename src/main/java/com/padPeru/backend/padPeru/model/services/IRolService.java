package com.padPeru.backend.padPeru.model.services;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.padPeru.backend.padPeru.model.entity.Role;

@Repository
public interface IRolService extends JpaRepository<Role, Integer>{
	Optional<Role> findByNombre(String nombre);
	boolean existsByNombre(String nombre);
}
