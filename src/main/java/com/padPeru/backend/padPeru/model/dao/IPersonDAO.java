package com.padPeru.backend.padPeru.model.dao;

import org.springframework.data.repository.CrudRepository;

import com.padPeru.backend.padPeru.model.entity.Person;

public interface IPersonDAO extends CrudRepository<Person, Long>{

}
