package com.padPeru.backend.padPeru.model.services.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.padPeru.backend.padPeru.model.entity.Role;
import com.padPeru.backend.padPeru.model.services.IRolService;import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RolService {
	
	@Autowired
	IRolService iRolService;
	
	public Optional<Role> getByNombre(String role){
		return iRolService.findByNombre(role);
	}
	
	public boolean existsByNombre(String role) {
		return iRolService.existsByNombre(role);
	}
	
	public void save(Role role) {
		iRolService.save(role);
	}
}
