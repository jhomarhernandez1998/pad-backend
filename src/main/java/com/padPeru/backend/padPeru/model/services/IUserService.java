package com.padPeru.backend.padPeru.model.services;

import java.util.List;

import com.padPeru.backend.padPeru.data.request.UserRequest;
import com.padPeru.backend.padPeru.model.entity.UserEntity;

import javassist.NotFoundException;

public interface IUserService {
	public List<UserEntity> findAll();
	public UserEntity findById(Long id);
	public UserEntity save(UserEntity user);
	// public User update(User user, UserRequest userRequest);
	// public User update(User user);
	public void delete(Long id);
	public UserEntity findByUsername(String username);
	public UserEntity findByEmail(String email);
	
}
