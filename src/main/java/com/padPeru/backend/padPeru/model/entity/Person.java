package com.padPeru.backend.padPeru.model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "persons")
public class Person {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/* TODO:dni */
	private String identification;
	
	/* TODO:nombres_completos */
	private String names;
	
	/* TODO:primer_apellido */
	private String first_name;
	
	/* TODO:segundo _apellido */
	private String last_name;
	
	/* TODO:Fecha_de_nacimiento */
	private String fec_nac;
	
	private String sexo;
	
	/* TODO:estado_civil */
	private String civil_status;
	
	/* TODO:direccion */
	private String direction;
	
	/* TODO:telefono */
	private String tlf;
	
	/* TODO:celular */
	private String cel;
	
	/* TODO:celular_de_emergencia */
	private String cel_emergency;


	/**
	 * @return the identification
	 */
	public String getIdentification() {
		return identification;
	}

	/**
	 * @param identification the identification to set
	 */
	public void setIdentification(String identification) {
		this.identification = identification;
	}

	/**
	 * @return the names
	 */
	public String getNames() {
		return names;
	}

	/**
	 * @param names the names to set
	 */
	public void setNames(String names) {
		this.names = names;
	}

	/**
	 * @return the first_name
	 */
	public String getFirst_name() {
		return first_name;
	}

	/**
	 * @param first_name the first_name to set
	 */
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	/**
	 * @return the last_name
	 */
	public String getLast_name() {
		return last_name;
	}

	/**
	 * @param last_name the last_name to set
	 */
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	/**
	 * @return the fec_nac
	 */
	public String getFec_nac() {
		return fec_nac;
	}

	/**
	 * @param fec_nac the fec_nac to set
	 */
	public void setFec_nac(String fec_nac) {
		this.fec_nac = fec_nac;
	}

	/**
	 * @return the sexo
	 */
	public String getSexo() {
		return sexo;
	}

	/**
	 * @param sexo the sexo to set
	 */
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	/**
	 * @return the civil_status
	 */
	public String getCivil_status() {
		return civil_status;
	}

	/**
	 * @param civil_status the civil_status to set
	 */
	public void setCivil_status(String civil_status) {
		this.civil_status = civil_status;
	}

	/**
	 * @return the direction
	 */
	public String getDirection() {
		return direction;
	}

	/**
	 * @param direction the direction to set
	 */
	public void setDirection(String direction) {
		this.direction = direction;
	}

	/**
	 * @return the tlf
	 */
	public String getTlf() {
		return tlf;
	}

	/**
	 * @param tlf the tlf to set
	 */
	public void setTlf(String tlf) {
		this.tlf = tlf;
	}

	/**
	 * @return the cel
	 */
	public String getCel() {
		return cel;
	}

	/**
	 * @param cel the cel to set
	 */
	public void setCel(String cel) {
		this.cel = cel;
	}

	/**
	 * @return the cel_emergency
	 */
	public String getCel_emergency() {
		return cel_emergency;
	}

	/**
	 * @param cel_emergency the cel_emergency to set
	 */
	public void setCel_emergency(String cel_emergency) {
		this.cel_emergency = cel_emergency;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	
	
}
