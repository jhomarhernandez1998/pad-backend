package com.padPeru.backend.padPeru.model.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.padPeru.backend.padPeru.model.dao.IStudentDAO;
import com.padPeru.backend.padPeru.model.entity.Student;
import com.padPeru.backend.padPeru.model.services.IStudentService;

@Service
public class StudentService implements IStudentService {
	@Autowired
	private IStudentDAO studentDAO;

	@Override
	public List<Student> findAll() {
		// TODO Auto-generated method stub
		return (List<Student>) studentDAO.findAll();
	}

	@Override
	public Student findById(Long id) {
		// TODO Auto-generated method stub
		return studentDAO.findById(id).orElse(null);
	}

	@Override
	public Student save(Student student) {
		// TODO Auto-generated method stub
		return studentDAO.save(student);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		studentDAO.deleteById(id);
	}
	
	
}
