package com.padPeru.backend.padPeru.model.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.padPeru.backend.padPeru.model.dao.IPersonDAO;
import com.padPeru.backend.padPeru.model.entity.Person;
import com.padPeru.backend.padPeru.model.services.IPersonService;

@Service
public class PersonService implements IPersonService{
	@Autowired
	private IPersonDAO personDAO;

	@Override
	public List<Person> findAll() {
		// TODO Auto-generated method stub
		return personDAOfindAll();
	}

	private List<Person> personDAOfindAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Person findById(Long id) {
		// TODO Auto-generated method stub
		return personDAO.findById(id).orElse(null);
	}

	@Override
	public Person save(Person person) {
		// TODO Auto-generated method stub
		return personDAO.save(person);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		personDAO.deleteById(id);
	}
	
	
}
