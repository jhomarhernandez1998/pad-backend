package com.padPeru.backend.padPeru.model.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.padPeru.backend.padPeru.model.entity.UserEntity;

public interface IUserDAO extends CrudRepository<UserEntity, Long>{
	/*@Query(value = "select * from users where id = ?", nativeQuery = true)
	public User findByUserId(Long id);*/
	
	public UserEntity findByEmail(String email);
	
	@Query(value = "select * from users where first_name = ?", nativeQuery = true)
	public UserEntity findByFirst_name(String username);
	
	//@Query("select u form users u where u.email")
	//public UserEntity findByEmail2(String email);

}
