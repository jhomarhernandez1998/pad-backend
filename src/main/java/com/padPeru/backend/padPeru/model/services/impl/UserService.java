package com.padPeru.backend.padPeru.model.services.impl;

import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.padPeru.backend.padPeru.data.request.UserRequest;
import com.padPeru.backend.padPeru.model.dao.IUserDAO;
import com.padPeru.backend.padPeru.model.entity.UserEntity;
import com.padPeru.backend.padPeru.model.services.IUserService;

@Service
public class UserService implements IUserService, UserDetailsService{
	
	private final Logger log = LoggerFactory.getLogger(UserService.class);
	
	@Autowired
	private IUserDAO userDAO;

	@Override
	public List<UserEntity> findAll() {
		// TODO Auto-generated method stub
		return (List<UserEntity>) userDAO.findAll();
	}

	@Override
	public UserEntity findById(Long id) {
		// TODO Auto-generated method stub
		return userDAO.findById(id).orElse(null);
	}

	@Override
	public UserEntity save(UserEntity user) {
		// TODO Auto-generated method stub
		return userDAO.save(user);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		userDAO.deleteById(id);		
	}

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		UserEntity userEntity = userDAO.findByEmail(username);
		//UserEntity userEntity = userDAO.findByEmail(username);
		
		if(userEntity== null) {
			String msg = "Error en el login: no existe el usuario con el email " + username;
			log.error(msg);
			throw new UsernameNotFoundException(msg);
		}
		List<GrantedAuthority> authorities = userEntity.getRoles()
				.stream()
				.map(role -> new SimpleGrantedAuthority(role.getNombre()))
				.peek(authority -> log.info("Role: " + authority.getAuthority()))
				.collect(Collectors.toList());

		return new User(userEntity.getFirst_name(), userEntity.getPass(), !userEntity.isDeleted(), true, true, true, authorities);
	}
	
	@Override
	@Transactional(readOnly = true)
	public UserEntity findByEmail(String email) {
		return userDAO.findByEmail(email);
	}

	@Override
	public UserEntity findByUsername(String username) {
		// TODO Auto-generated method stub
		return userDAO.findByFirst_name(username);
	}
	
}
