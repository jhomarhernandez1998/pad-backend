package com.padPeru.backend.padPeru.model.services;

import java.util.List;

import com.padPeru.backend.padPeru.model.entity.Person;

public interface IPersonService {
	public List<Person> findAll();
	public Person findById(Long id);
	public Person save(Person person);
	public void delete(Long id);
}
