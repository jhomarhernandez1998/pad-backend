package com.padPeru.backend.padPeru.model.dao;

import org.springframework.data.repository.CrudRepository;

import com.padPeru.backend.padPeru.model.entity.Teacher;


public interface ITeacherDAO extends CrudRepository<Teacher, Long>{

}
