package com.padPeru.backend.padPeru.model.services;

import java.util.List;

import com.padPeru.backend.padPeru.model.entity.Student;


public interface IStudentService {
	public List<Student> findAll();
	public Student findById(Long id);
	public Student save(Student student);
	public void delete(Long id);
}
