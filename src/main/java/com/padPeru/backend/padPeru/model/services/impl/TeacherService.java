package com.padPeru.backend.padPeru.model.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.padPeru.backend.padPeru.model.dao.ITeacherDAO;
import com.padPeru.backend.padPeru.model.entity.Teacher;
import com.padPeru.backend.padPeru.model.services.ITeacherService;

@Service
public class TeacherService implements ITeacherService{
	@Autowired
	private ITeacherDAO teacherDAO;

	@Override
	public List<Teacher> findAll() {
		// TODO Auto-generated method stub
		return (List<Teacher>) teacherDAO.findAll();
	}

	@Override
	public Teacher findById(Long id) {
		// TODO Auto-generated method stub
		return teacherDAO.findById(id).orElse(null);
	}

	@Override
	public Teacher save(Teacher teacher) {
		// TODO Auto-generated method stub
		return teacherDAO.save(teacher);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		teacherDAO.deleteById(id);
	}

}
