package com.padPeru.backend.padPeru.auth;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import com.padPeru.backend.padPeru.model.entity.UserEntity;
import com.padPeru.backend.padPeru.model.services.IUserService;

@Component
public class InfoAdicionalToken implements TokenEnhancer{
	
	private final Logger log = LoggerFactory.getLogger(InfoAdicionalToken.class);
	
	@Autowired
	private IUserService userService;
	
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
		log.info("Nombre de usuario " + authentication.getName());
		
		UserEntity user = userService.findByUsername(authentication.getName());
		
		Map<String, Object> info = new HashMap<>();
		info.put("info_adicional", "Hola que tal!: ".concat(authentication.getName()));
		
		info.put("nombre", user.getFirst_name());
		info.put("apellido", user.getLast_name());
		info.put("email", user.getEmail());
		
		((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(info);
		
		return accessToken;
	}
	
}
