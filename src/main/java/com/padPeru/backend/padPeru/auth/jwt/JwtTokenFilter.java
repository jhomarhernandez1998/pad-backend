package com.padPeru.backend.padPeru.auth.jwt;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.padPeru.backend.padPeru.model.entity.UserEntity;
import com.padPeru.backend.padPeru.model.services.IUserService;
import com.padPeru.backend.padPeru.model.services.impl.UserService;

public class JwtTokenFilter extends OncePerRequestFilter{
	
	private final static Logger logger = LoggerFactory.getLogger(JwtTokenFilter.class);

	@Autowired
	JwtProvider jwtProvider;
	
	@Autowired
	private UserService userService;
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			String token = getToken(request);
			
			String email = jwtProvider.getEmailFromToken(token);
			
			UserDetails user = userService.loadUserByUsername(email);
			
			UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(email, null, user.getAuthorities());
			
			SecurityContextHolder.getContext().setAuthentication(auth);
	
		}catch(Exception e) {
			logger.error("fail en el método doFilter");
			//e.printStackTrace();
		}
		chain.doFilter(request, response);
	}
	
	private String getToken(HttpServletRequest req) {
		String authReq = req.getHeader("Authorization");
		if(authReq != null && authReq.startsWith("Bearer "))
			return authReq.replace("Bearer ", "");
		return null;
	}
	
}
