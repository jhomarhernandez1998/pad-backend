package com.padPeru.backend.padPeru;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
public class PadPeruApplication implements CommandLineRunner{
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	public static void main(String[] args) {
		SpringApplication.run(PadPeruApplication.class, args);
	}

	
	@Override
	public void run(String... args) throws Exception {
		String password = "12345";
		String password2 = "askdjfklajsdklf53Tt1254Tasfdsafasdfku";
		for (int i = 0; i < 4; i++) {
			String passwordBcrypt = passwordEncoder.encode(password);
			System.out.println(passwordBcrypt);
		}
		String passwordBcrypt2 = passwordEncoder.encode(password2);
		System.out.println(passwordBcrypt2);
	}
}
