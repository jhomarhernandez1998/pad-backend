package com.padPeru.backend.padPeru.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.padPeru.backend.padPeru.model.entity.UserEntity;
import com.padPeru.backend.padPeru.model.services.impl.UserService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/user")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@GetMapping("/lister")
	public List<UserEntity> index(){
		return userService.findAll();
	}
	
	@GetMapping("/filter/{id}")
	public ResponseEntity<?> findById(@PathVariable Long id){
		Map<String, Object> response = new HashMap<>();
		UserEntity user = null;
		try {
			user = userService.findById(id);
		} catch (DataAccessException e) {
			// TODO: handle exception
			response.put("Message", "Error al realizar la consulta");
			response.put("Error", e.getMessage().concat("==").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(user == null) {
			response.put("Message", "User ID: ".concat(id.toString().concat(" no existe en la Base de Datos")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	
	@PostMapping("/create")
	public ResponseEntity<?> createUser(
			@RequestBody UserEntity user,
			BindingResult result
	) {
		Map<String, Object> response = new HashMap<>();
		UserEntity userNew = null;
		
		if(result.hasErrors()) {
			List<String> errors = result.getFieldErrors()
					.stream()
					.map(err -> "El campo: "+ err.getDefaultMessage())
					.collect(Collectors.toList());
			response.put("Errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
		try {
			userNew = userService.save(user);
		} catch (DataAccessException e) {
			// TODO: handle exception
			response.put("Message", "Error al realizar la insercion");
			response.put("Error", e.getMessage().concat("==").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("Message", "User guardado con exito");
		response.put("Message", userNew);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<?> updateUser(
			@RequestBody UserEntity user,
			BindingResult result,
			@PathVariable Long id
	) {
		Map<String, Object> response = new HashMap<>();
		UserEntity userCurrent = userService.findById(id);
		UserEntity userUpdate = null;
		
		if(result.hasErrors()) {
			List<String> errors = result.getFieldErrors()
					.stream()
					.map(err -> "El campo: "+ err.getDefaultMessage())
					.collect(Collectors.toList());
			response.put("Errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
		if(userCurrent == null) {
			response.put("Message", "Error: no se puede editar ID: ".concat(id.toString().concat(" no existe en la base de datos")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		
		try {
			userCurrent.setEmail(user.getEmail());
			userCurrent.setFacebook_id(user.getFacebook_id());
			userCurrent.setFirst_name(user.getFirst_name());
			userCurrent.setGoogle_id(user.getGoogle_id());
			userCurrent.setLast_name(user.getLast_name());
			userCurrent.setPass(user.getPass());
			userCurrent.setPicture(user.getPicture());
			userUpdate = userService.save(userCurrent);
		} catch (DataAccessException e) {
			// TODO: handle exception
			response.put("Message", "Error al realizar la actualizacion");
			response.put("Error", e.getMessage().concat("==").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("Message", "User actualizado con exito");
		response.put("Message", userUpdate);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deleteUser(
			@PathVariable Long id
	) {
		Map<String, Object> response = new HashMap<>();
		
		try {
			UserEntity user = userService.findById(id);
			userService.delete(id);
		} catch (DataAccessException e) {
			// TODO: handle exception
			response.put("Message", "Error al realizar la eliminacion");
			response.put("Error", e.getMessage().concat("==").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("Message", "User eliminado con exito");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
}
