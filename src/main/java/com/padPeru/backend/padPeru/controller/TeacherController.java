package com.padPeru.backend.padPeru.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.padPeru.backend.padPeru.model.entity.Teacher;
import com.padPeru.backend.padPeru.model.services.impl.TeacherService;

@RestController
@RequestMapping("/api/teacher")
public class TeacherController {
	@Autowired
	private TeacherService teacherService;
	
	@GetMapping("/lister")
	public List<Teacher> index(){
		return teacherService.findAll();
	}
	
	@GetMapping("/filter/{id}")
	public ResponseEntity<?> findById(
			@PathVariable Long id
	){
		Map<String, Object> response = new HashMap<>();
		Teacher teacher = null;
		try {
			teacher = teacherService.findById(id);
		} catch (DataAccessException e) {
			// TODO: handle exception
			response.put("Message", "Error al realizar la consulta");
			response.put("Error", e.getMessage().concat("==").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(teacher == null) {
			response.put("Message", "teacher ID: ".concat(id.toString().concat(" no existe en la Base de Datos")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	@PostMapping("/create")
	public ResponseEntity<?> createTeacher(
			@RequestBody Teacher teacher,
			BindingResult result
	) {
		Map<String, Object> response = new HashMap<>();
		Teacher teacherNew = null;
		
		if(result.hasErrors()) {
			List<String> errors = result.getFieldErrors()
					.stream()
					.map(err -> "El campo: "+ err.getDefaultMessage())
					.collect(Collectors.toList());
			response.put("Errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
		try {
			teacherNew = teacherService.save(teacher);
		} catch (DataAccessException e) {
			// TODO: handle exception
			response.put("Message", "Error al realizar la insercion");
			response.put("Error", e.getMessage().concat("==").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("Message", "teacher guardado con exito");
		response.put("Message", teacherNew);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<?> updateTeacher(
			@RequestBody Teacher teacher,
			BindingResult result,
			@PathVariable Long id
	) {
		Map<String, Object> response = new HashMap<>();
		Teacher teacherCurrent = teacherService.findById(id);
		Teacher teacherUpdate = null;
		
		if(result.hasErrors()) {
			List<String> errors = result.getFieldErrors()
					.stream()
					.map(err -> "El campo: "+ err.getDefaultMessage())
					.collect(Collectors.toList());
			response.put("Errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
		if(teacherCurrent == null) {
			response.put("Message", "Error: no se puede editar ID: ".concat(id.toString().concat(" no existe en la base de datos")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		
		try {
			teacherCurrent.setIdentification(teacher.getIdentification());
			teacherCurrent.setNames(teacher.getNames());
			teacherCurrent.setFirst_name(teacher.getFirst_name());
			teacherCurrent.setLast_name(teacher.getLast_name());
			teacherCurrent.setFec_nac(teacher.getFec_nac());
			teacherCurrent.setSexo(teacher.getSexo());
			teacherCurrent.setCivil_status(teacher.getCivil_status());
			teacherCurrent.setDirection(teacher.getDirection());
			teacherCurrent.setTlf(teacher.getTlf());
			teacherCurrent.setCel(teacher.getCel());
			teacherCurrent.setCel_emergency(teacher.getCel_emergency());
			teacherUpdate = teacherService.save(teacherCurrent);
		} catch (DataAccessException e) {
			// TODO: handle exception
			response.put("Message", "Error al realizar la actualizacion");
			response.put("Error", e.getMessage().concat("==").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("Message", "teacher actualizado con exito");
		response.put("Message", teacherUpdate);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deleteTeacher(
			@PathVariable Long id
	) {
		Map<String, Object> response = new HashMap<>();
		
		try {
			Teacher teacher = teacherService.findById(id);
			teacherService.delete(id);
		} catch (DataAccessException e) {
			// TODO: handle exception
			response.put("Message", "Error al realizar la eliminacion");
			response.put("Error", e.getMessage().concat("==").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("Message", "teacher eliminado con exito");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
}
