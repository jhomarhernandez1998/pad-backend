package com.padPeru.backend.padPeru.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.User;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.padPeru.backend.padPeru.auth.InfoAdicionalToken;
import com.padPeru.backend.padPeru.auth.jwt.JwtProvider;
import com.padPeru.backend.padPeru.dto.TokenDTO;
import com.padPeru.backend.padPeru.model.entity.Role;
import com.padPeru.backend.padPeru.model.entity.UserEntity;
import com.padPeru.backend.padPeru.model.services.IRolService;
import com.padPeru.backend.padPeru.model.services.impl.RolService;
import com.padPeru.backend.padPeru.model.services.impl.UserService;

@RestController
@RequestMapping("/oauth")
@CrossOrigin
public class OauthController {
	
	private final Logger log = LoggerFactory.getLogger(OauthController.class);
	
	@Value("${google.clientId}")
	String googleClientId;
	
	
	@Value("${secretPsw}")
	String secretPsw;
	
	@Autowired
	RolService rolService;
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Autowired
	AuthenticationManager authenticationManager;
	
	@Autowired
	JwtProvider jwtProvider;
	
	@Autowired
	UserService userService;
	
	@PostMapping("/google")
	public ResponseEntity<?> google(@RequestBody TokenDTO tokenDTO) throws IOException{
		final NetHttpTransport transport = new NetHttpTransport();
		final JacksonFactory jackson = new JacksonFactory().getDefaultInstance();
		GoogleIdTokenVerifier.Builder verifier = 
				new GoogleIdTokenVerifier.Builder(transport, jackson)
					.setAudience(Collections.singletonList(googleClientId));
		final GoogleIdToken googleIdToken = GoogleIdToken.parse(verifier.getJsonFactory(), tokenDTO.getValue());
		final GoogleIdToken.Payload payload = googleIdToken.getPayload();
		UserEntity user = new UserEntity();
		if (userService.findByEmail(payload.getEmail())!=null) {
			user = userService.findByEmail(payload.getEmail());
		} else {
			return new ResponseEntity("Error en las credenciales", HttpStatus.UNAUTHORIZED);
		}
		user.setPicture(payload.get("picture").toString());
		userService.save(user);
		TokenDTO tokenRes = login(user);
		return new ResponseEntity(tokenRes, HttpStatus.OK);
	}
	
	@PostMapping("/facebook")
	public ResponseEntity<?> facebook(@RequestBody TokenDTO tokenDTO){
		Facebook facebook = new FacebookTemplate(tokenDTO.getValue());
		final String[] fields = {"email", "picture"};
		User user = facebook.fetchObject("me", User.class, fields);
		UserEntity userEntity = new UserEntity();
		if (userService.findByEmail(user.getEmail())!=null) {
			userEntity = userService.findByEmail(user.getEmail());
		} else {
			return new ResponseEntity("Error en las credenciales", HttpStatus.UNAUTHORIZED);
		} 
		userEntity.setPicture("http://graph.facebook.com/" + user.getId() + "/picture?access_token=" + tokenDTO.getValue());
		TokenDTO tokenRes = login(userEntity);
		return new ResponseEntity(tokenRes, HttpStatus.OK);
	}
	
	private TokenDTO login(UserEntity user) {
		Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getEmail(), secretPsw));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtProvider.generateToken(authentication);
		TokenDTO tokenDTO = new TokenDTO();
		tokenDTO.setValue(jwt);
		return tokenDTO;
	}
	
	@PostMapping("/create/")
	private UserEntity saveUsuario(String email) {
		UserEntity user = new UserEntity();
		user.setEmail(email);
		user.setPass(passwordEncoder.encode(secretPsw));
		Role rolUser = rolService.getByNombre("ROLE_ALUMNO").get();
		ArrayList<Role> roles = new ArrayList();
		roles.add(rolUser);
		user.setRoles(roles);
		return userService.save(user);
	}
}
