package com.padPeru.backend.padPeru.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.padPeru.backend.padPeru.model.entity.Student;
import com.padPeru.backend.padPeru.model.services.IStudentService;

@RestController
@RequestMapping("/api/student")
public class StudentController {
	@Autowired
	private IStudentService studentService;
	
	@GetMapping("/lister")
	public List<Student> index(){
		return studentService.findAll();
	}
	
	/*@GetMapping("/filter/{id}")
	public ResponseEntity<?> findById(
			@PathVariable Long id
	){
		Map<String, Object> response = new HashMap<>();
		Student person = null;
		try {
			person = studentService.findById(id);
		} catch (DataAccessException e) {
			// TODO: handle exception
			response.put("Message", "Error al realizar la consulta");
			response.put("Error", e.getMessage().concat("==").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(person == null) {
			response.put("Message", "Student ID: ".concat(id.toString().concat(" no existe en la Base de Datos")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	@PostMapping("/create")
	public ResponseEntity<?> createPerson(
			@RequestBody Teacher person,
			BindingResult result
	) {
		Map<String, Object> response = new HashMap<>();
		Teacher personNew = null;
		
		if(result.hasErrors()) {
			List<String> errors = result.getFieldErrors()
					.stream()
					.map(err -> "El campo: "+ err.getDefaultMessage())
					.collect(Collectors.toList());
			response.put("Errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
		try {
			personNew = personService.save(person);
		} catch (DataAccessException e) {
			// TODO: handle exception
			response.put("Message", "Error al realizar la insercion");
			response.put("Error", e.getMessage().concat("==").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("Message", "person guardado con exito");
		response.put("Message", personNew);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<?> updatePerson(
			@RequestBody Teacher person,
			BindingResult result,
			@PathVariable Long id
	) {
		Map<String, Object> response = new HashMap<>();
		Teacher personCurrent = personService.findById(id);
		Teacher personUpdate = null;
		
		if(result.hasErrors()) {
			List<String> errors = result.getFieldErrors()
					.stream()
					.map(err -> "El campo: "+ err.getDefaultMessage())
					.collect(Collectors.toList());
			response.put("Errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
		if(personCurrent == null) {
			response.put("Message", "Error: no se puede editar ID: ".concat(id.toString().concat(" no existe en la base de datos")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		
		try {
			personCurrent.setIdentification(person.getIdentification());
			personCurrent.setNames(person.getNames());
			personCurrent.setFirst_name(person.getFirst_name());
			personCurrent.setLast_name(person.getLast_name());
			personCurrent.setFec_nac(person.getFec_nac());
			personCurrent.setSexo(person.getSexo());
			personCurrent.setCivil_status(person.getCivil_status());
			personCurrent.setDirection(person.getDirection());
			personCurrent.setTlf(person.getTlf());
			personCurrent.setCel(person.getCel());
			personCurrent.setCel_emergency(person.getCel_emergency());
			personUpdate = personService.save(personCurrent);
		} catch (DataAccessException e) {
			// TODO: handle exception
			response.put("Message", "Error al realizar la actualizacion");
			response.put("Error", e.getMessage().concat("==").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("Message", "person actualizado con exito");
		response.put("Message", personUpdate);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deletePerson(
			@PathVariable Long id
	) {
		Map<String, Object> response = new HashMap<>();
		
		try {
			Teacher person = personService.findById(id);
			personService.delete(id);
		} catch (DataAccessException e) {
			// TODO: handle exception
			response.put("Message", "Error al realizar la eliminacion");
			response.put("Error", e.getMessage().concat("==").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("Message", "person eliminado con exito");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}*/
}
