package com.padPeru.backend.padPeru.data.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserResponse {
	// TODO: @NotNull
	@JsonProperty("email")
	private String email;
	
	@JsonProperty("pass")
	private String pass;
	
	@JsonProperty("picture")
	private String picture;
	
	@JsonProperty("first_name")
	private String first_name;
	
	@JsonProperty("last_name")
	private String last_name;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	
	
}
