package com.padPeru.backend.padPeru.data;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponsesEntities {
	public ResponseEntity<?> responseEntityOkClass(Object object){
		return new ResponseEntity<>(object, HttpStatus.OK);
	}
	
	public ResponseEntity<Map<String, Object>> responseEntityDelete(){
		Map<String, Object> response = new HashMap<>();
		response.put("Message", "Delete Succefully");
		return new ResponseEntity<Map<String,Object>>(response, HttpStatus.OK);
	}
	
	public ResponseEntity<Map<String, Object>> responseEntityNoyFound(String className){
		Map<String, Object> response = new HashMap<>();
		response.put(className, response);
		return new ResponseEntity<Map<String,Object>>(response, HttpStatus.OK);
	}
}
