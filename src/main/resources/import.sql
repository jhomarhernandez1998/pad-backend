
-- users

insert into users (email, pass, first_name, last_name) values('demo@gmail.com', '$2a$10$RJPuqMVeFdH0mCxN4hUdr.KQ.7a/SoTpnemnqhKqvwPCATTq6hhBq', 'nombre', 'apellido');
insert into users (email, pass, first_name, last_name, picture) values('jhomar.hernandez1998@gmail.com', '$2a$10$IAJGbyRufJRM7lv1Ut4CrOlfsA7v/fjIOV1rsNkMdKIa.I1RyD4Ua', 'jhomar', 'hernandez','');
insert into users (email, pass, first_name, last_name, picture) values('jhomar_9_para100pre@hotmail.com', '$2a$10$AKFGXUWglgLEfsK.G05ec.3Qq1Ax3yf8PufiDw2zkltEvO.PLgekG', 'jhomar de facebook', 'hernandez','');

-- Roles
INSERT INTO roles (nombre) VALUES ('ROLE_ALUMNO');
INSERT INTO roles (nombre) VALUES ('ROLE_PROFESOR');
INSERT INTO roles (nombre) VALUES ('ROLE_ADMIN');

INSERT INTO users_role (user_id, role_id) values (1, 1);
INSERT INTO users_role (user_id, role_id) values (1, 2);
INSERT INTO users_role (user_id, role_id) values (2, 1);
INSERT INTO users_role (user_id, role_id) values (2, 2);

